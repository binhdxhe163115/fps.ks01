﻿using System;
using System.Text;

public class Program
{
    static public int FindMaxSubArray(int[] inputArray, int subLength)
    {
        // Khởi tạo biến maxSum với giá trị nhỏ nhất 
        int maxSum = int.MinValue;

        // Duyệt qua từng array có độ dài là subLength
        for (int i = 0; i <= inputArray.Length - subLength; i++)
        {
            // Biến để tính tổng của dãy con có độ dài subLength
            int currentSum = 0;

            // Tính tổng của dãy con có độ dài subLength kể từ vị trí i
            for (int j = i; j < i + subLength; j++)
            {
                currentSum += inputArray[j];
            }

            // So sánh và cập nhật tổng lớn nhất
            maxSum = Math.Max(maxSum, currentSum);
        }

        // Trả về tổng lớn nhất của subarray
        return maxSum;
    }

    static void Main(string[] args)
    {

        Console.OutputEncoding = Encoding.UTF8;
        // Nhập mảng từ người dùng
        Console.WriteLine("Nhập các phần tử của mảng, cách nhau bằng dấu phẩy:");
        string[] inputArrayStr = Console.ReadLine().Split(',');

        // Tạo một mảng số nguyên có cùng kích thước với mảng chuỗi để lưu các phần tử của mảng dưới dạng số nguyên
        int[] inputArray = new int[inputArrayStr.Length];

        // Duyệt qua từng phần tử trong mảng chuỗi để chuyển đổi từng chuỗi thành số nguyên và lưu vào các mảng tương ứng
        for (int i = 0; i < inputArray.Length; i++)
        {
            inputArray[i] = int.Parse(inputArrayStr[i]);
        }

        // Nhập độ dài của dãy con từ người dùng
        Console.WriteLine("Nhập độ dài của dãy con:");
        int subLength = int.Parse(Console.ReadLine());

        // Gọi phương thức FindMaxSubArray để tìm tổng lớn nhất của dãy con
        int result = FindMaxSubArray(inputArray, subLength);

        // In ra kết quả
        Console.WriteLine("Output " + result);
    }
}
