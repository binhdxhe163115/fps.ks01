﻿using System;
using System.Text;

public class Program
{
    static public string GetArticleSummary(string content, int maxLength)
    {
        // Kiểm tra độ dài của context
        if (content.Length <= maxLength)
        {
            // Trả về context ban đầu nếu độ dài không vượt quá maxLength
            return content;
        }
        else
        {
            // Tìm vị trí cắt cuối cùng trước maxLength và không cắt ngay giữa từ
            int lastSpaceIndex = content.LastIndexOf(' ', maxLength);

            // Tạo summary bằng cách cắt context đến vị trí cắt và thêm dấu '...'
            string summary = content.Substring(0, lastSpaceIndex) + "...";

            // Trả về summary
            return summary;
        }
    }

    static void Main(string[] args)
    {

        Console.OutputEncoding = Encoding.UTF8;
        // Nhập context từ người dùng và kiểm tra dữ liệu
        string content;
        do
        {
            Console.WriteLine("Nhập nội dung bài báo:");
            content = Console.ReadLine();
            //Trường hợp nội dung nhập bị trống => yêu cầu người dùng nhập lại
            if (string.IsNullOrWhiteSpace(content))
            {
                Console.WriteLine("Nội dung không được để trống. Vui lòng nhập lại.");
            }
        } while (string.IsNullOrWhiteSpace(content));

        // Nhập độ dài tối đa cho phép của summary từ người dùng và kiểm tra dữ liệu
        int maxLength;
        do
        {
            Console.WriteLine("Nhập độ dài tối đa cho summary:");
            string maxLengthInput = Console.ReadLine();
            //Trường hợp nội dung nhập bị trống hoặc không phải là số nguyên dương => yêu cầu người dùng nhập lại
            if (!int.TryParse(maxLengthInput, out maxLength) || maxLength <= 0)
            {
                Console.WriteLine("Độ dài tối đa phải là một số nguyên dương. Vui lòng nhập lại.");
            }
        } while (maxLength <= 0);

        // Gọi method để tóm tắt context và hiển thị kết quả
        Console.WriteLine("Summary: \"" + GetArticleSummary(content, maxLength) + "\"");
    }
}
